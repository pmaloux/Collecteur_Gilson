
#! python2
# -*-coding: 'utf-8' -*
'''This module needs the GSIOC implementation and handles the module devices'''


class Device():
    '''General methods relative to GSIOC-controlled devices'''

    def __init__(self, gsioc, module_id):
        self.module_id = module_id
        self.gsioc = gsioc

    def immediate_command(self, command):
        '''Immediate command'''
        return self.gsioc.immediate_command(self.module_id, command)

    def buffered_command(self, command):
        '''Buffered command'''
        return self.gsioc.buffered_command(self.module_id, command)

    def identification(self):
        '''Return the identification string of the module'''
        return self.immediate_command("%")

    def reset(self):
        '''Resets the device'''
        return self.immediate_command("$")

    def status(self):
        '''Return the status of the device'''
        return self.immediate_command("?")


class SystemController(Device):
    '''Methods to manage the 506C Gilson System Interface via GSIOC'''

    def get_analog_values(self, contact_input):
        'Return the analog value of the selected contact_input as a floating decimal number'
        return float(self.immediate_command(contact_input)[2:8])


class FractionController(Device):
    '''Methods to manage a 201-202 Gilson Fraction Controller via GSIOC'''
    keyboard = {'home': 'H',\
        'start': 'B',\
        'enter': 'E',\
        'clear': 'C',\
        'advance': 'A',\
        'delete': 'D',\
        'drop': 'G',\
        'reset': 'I',\
        'manual': 'M',\
        'peak': 'P',\
        'pause': 'Q',\
        'drain': 'R',\
        'signal': 'V',\
        'signal_prog': 'S',\
        'time': 'T',\
        'edit': 'U',\
        'time_prog': 'Z'}

    rack_10 = {'1': (9940, 14111),\
        '2': (14393, 14111),\
        '3': (18846, 14111),\
        '4': (23299, 14111),\
        '5': (27752, 14111),\
        '6': (32205, 14111),\
        '7': (36658, 14111),\
        '8': (41111, 14111),\
        '9': (45564, 14111),\
        '10': (50017, 14111),\
        '11': (54470, 14111),\
        '12': (58923, 14111),\
        '13': (63376, 14111),\
        '14': (63376, 36059),\
        '15': (58923, 36059)}

    def get_well(self):
        '''Give well number according to the collector position'''
        x_position = float(self.immediate_command('X'))
        y_position = float(self.immediate_command('Y'))

        if x_position == 0:
            return 'HOME'
        elif y_position == 25085:
            return 'DRAIN'
        else:
            well = next((well for well, position in self.rack_10.items() \
                if position == (x_position, y_position)), None)

            if well:
                return well
        return False

    def set_status(self, mode):
        '''Set the communication status of the master'''
        if mode == 'keypad':
            mode_to_send = "SK"
        elif mode == 'remote':
            mode_to_send = "SR"
        elif mode == 'primitive':
            mode_to_send = "SP"
        else:
            print("Unknown mode. Possible: keypad, remote, primitive.")
            return False

        return self.buffered_command(mode_to_send)



class Fc203B(Device):
    '''Methods to manage a 201-202 Gilson Fraction Controller via GSIOC'''


    def gotofrac(self,fracnumber):
        '''Moves the head above the specified fraction'''
        command = 'T00'+str(fracnumber)
        self.buffered_command(command)

    def nextfrac(self):
        '''moves the head to the next fraction following the path specified by the rack number'''
        self.buffered_command('KA')

    def collectionreset(self):
        '''resets the position of the head to the home position'''
        self.buffered_command('KE')

    def startcollection(self):
        '''moves the head above the first fraction'''
        self.buffered_command('T001')

    def beep(self, duration):
        '''beeps...'''
        self.buffered_command(str(duration))



