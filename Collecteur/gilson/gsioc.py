﻿#! python2
# -*-coding: 'utf-8' -*
'''This module is an attempt timeo implement the GSIOC protocol in Python3'''

import serial
import time
#from exceptions import DeviceDoesntAnswer

# Binary values of a selection of characters
DISCONNECT = b'\xff'
ERROR_POUND = b'\x23'
ACK = b'\x06'
LINE_FEED = b'\x0A'
CARRIAGE_RETURN = b'\x0D'


class SerialComm():
    '''This class handles the communication via the serial port'''

    def __init__(self, port, baudrate=19200):
        '''Configuration of the RS-232 connection. No arguments accepted.'''
        self.ser = serial.Serial()
        self.ser.port = port # = COM"X" - 1 for W$, /dev/ttySX for Linux
        self.ser.baudrate = baudrate
        self.ser.bytesize = 8
        self.ser.parity = 'E'
        self.ser.stopbits = 1
        self.ser.timeout = 0.02 #Read timeout
        # self.ser.xonxoff = 1
        self.ser.rtscts = 1

        try:
            self.ser.open()
            print("Serial port opened\n")
        except serial.serialutil.SerialException:
            print("Unable to open serial port. Hint: check if the COM port is the right one...\n")

    def close_serial(self):
        '''Close the serial port, no argument needed.'''
        self.ser.close()
        if self.ser.isOpen():
            print("Unable to close serial port\n")
        else:
            print("Serial port closed! Have a good day.\n")

    def send_byte(self, data):
        '''Write a byte on the serial port.'''
        return self.ser.write(data)

    def send_int(self, data):
        '''Convert int->bytearray and send it on serial port.'''
        return self.ser.write(bytearray([data]))

    def send_bin(self, data):
    	'''Convert int->binary and send it on serial port'''
    	return self.ser.write(bin([data]))

    def send_str(self, data):
        '''Convert str/char->byte and send it on serial port.'''
        return self.ser.write(bytearray(data.encode('ascii')))

    def get_byte(self):
        '''Read on the serial port and return exactly one byte.'''
        return self.ser.read(1)


class GsiocHandler():
    '''
    Python implementation of the Gilson Serial Input/Output Channel protocol.
    Handles the serial port communication : connection, disconnection,
    immediate and buffered command.
    '''

    max_retries_connect = 5
    connected = 0

    def __init__(self, port='COM3', baudrate=19200):
        '''Opens the way for communication with serial port'''
        self.serial = SerialComm(port, baudrate)

    def connect_id(self, module_id):
        '''
        Connect the wanted ID module.
        Answer immediately True if the module is already connected, False if
        the connection is impossible.
        The termination mode on the 506C has to be passive.
        '''
        if self.connected == module_id:
            return True

        for _ in range(self.max_retries_connect):
            self.serial.send_byte(DISCONNECT)
            time.sleep(0.02)

            self.serial.send_int(module_id+128)
            ret_byte = self.serial.get_byte()

            if not ret_byte:
                print("Device ID:{0} doesn't answer...".format(module_id))
                self.connected = 0
            elif ord(ret_byte) == module_id+128:
                print("Device ID:{} has answered and is connected"\
                    .format(module_id))
                self.connected = module_id
                return True
        #raise DeviceDoesntAnswer("Impossible to connect the module")

    def immediate_command(self, module_id, command_ascii):
        '''
        Send an immediate command to the desired module #ID.
        Return the answer of the module.
        '''
        self.connect_id(module_id)

        self.serial.send_str(command_ascii)
        comm_done = False
        answer = bytes()

        while comm_done is not True:
            ret_byte = self.serial.get_byte()
            print("Retour:", ret_byte)   #PB

            if ret_byte == ERROR_POUND:
                print("Device ID:{} doesn't know '{}'"\
                    .format(module_id, command_ascii))
                return False
            elif ord(ret_byte) < 128:
                answer += ret_byte
                self.serial.send_byte(ACK)
            else:
                answer = answer.decode('ascii') + chr(ord(ret_byte)-128)
                comm_done = True
                return answer

    def buffered_command(self, module_id, command_ascii):
        '''
        Send a buffered command to the desired module #ID. The module doesn't
        have to answer to a buffered command.
        '''
        self.connect_id(module_id)

        ready_to_listen = False

        while ready_to_listen is False:
            self.serial.send_byte(LINE_FEED)
            ret_byte = self.serial.get_byte()

            if ret_byte == ERROR_POUND:
                print("Device:{} is busy. Let's wait a bit..."\
                    .format(module_id))
                time.sleep(0.5) # Time before we retry to send the command
            elif ret_byte == LINE_FEED:
                ready_to_listen = True

        for char_ascii in command_ascii:
            self.serial.send_str(char_ascii)
            ret_byte = self.serial.get_byte()

            if ret_byte != char_ascii.encode('ascii'):
                print("Device:{} didn't confirm {}"\
                    .format(module_id, char_ascii))
                return False

        self.serial.send_byte(CARRIAGE_RETURN)
        ret_byte = self.serial.get_byte()

        if ret_byte == CARRIAGE_RETURN:
            return True
        return False

