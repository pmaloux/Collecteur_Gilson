import time
import gsioc
from devices import SystemController, Fc203B

# ------ INITILISATION ------- #

GSIOC = gsioc.GsiocHandler(port='COM3')
SC = SystemController(GSIOC, 63)
FC = Fc203B(GSIOC, 0)

time.sleep(0.5)
SC.identification()
time.sleep(0.5)
FC.identification()

FC.buffered_command('00400') #sets buffered_command's control rate in 0,01 Hz
FC.collectionreset()
time.sleep(2)


# ---------- LAUNCH ---------- #

FC.buffered_command('KS')
FC.gotofrac(1)
FC.nextfrac()
