﻿#! python3
# -*-coding: 'utf-8' -*
'''This module contains the custom exceptions for my GSIOC implementation'''

class DeviceDoesntAnswer(Exception):
    pass
    
    
class ExperimentAlreadyExists(Exception):
    pass