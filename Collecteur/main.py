#! python2
# -*-coding: 'utf-8' -*
"""Main module GSIOC"""
import time
import gilson.gsioc
from gilson.devices import SystemController, FractionController

GSIOC = gilson.gsioc.GsiocHandler(port='COM3')
SC = SystemController(GSIOC, 63)
FC = FractionController(GSIOC, 0)

time.sleep(1)
SC.status()
# print('status de SC', SC.status())

# time.sleep(1)
# FC.status()